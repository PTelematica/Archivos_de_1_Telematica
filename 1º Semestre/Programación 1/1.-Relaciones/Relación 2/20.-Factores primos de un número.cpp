#include <iostream>
using namespace std;

bool esPrimo (int n);

int main (){
	int n, i=2, mult=1;
	
	cout<<"Introduzca un n�mero"<<endl;
	cin>>n;
	
	while ((i<=n)&&(mult!=n)){
		if (esPrimo(i)){
			while (n%i==0){
				cout<<i<<"  ";
				mult*=i;
				n/=i;
			}
		}
		i++;
		}
return 0;
}

bool esPrimo (int n){
	int i=2;
	bool primo=true;
	
	while ((i<n)&&(primo)){
		if (n%i==0){
			primo=false;
		}
		i++;
	}
return primo;
}
