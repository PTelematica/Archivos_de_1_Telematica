#include <iostream>
using namespace std;

float suma (float a,float b);
float resta (float a,float b);
float mul (float a,float b);
float div (float a,float b);

int main (){
	float a,b;
	char c;
	
	cout<<"Introduzca dos n�meros naturales"<<endl;
	cin>>a;
	if (a>=0){
		cin>>b;
		if (b>=0){
			cout<<"Introduzca la operaci�n a realizar [+,-,*,/]"<<endl;
			cin>>c;
			
			switch (c){
				case '+':
					cout<<suma(a,b);
					break;
				case '-':
					cout<<resta(a,b);
					break;
				case '*':
					cout<<mul(a,b);
					break;
				case '/':
					cout<<div(a,b);
					break;
				default:
					cout<<"No es posible realizar la operaci�n propuesta"<<endl;
			}
		}else{
			cout<<"S�lamente se permiten n�meros naturales"<<endl;
		}
	}else{
		cout<<"S�lamente se permiten n�meros naturales"<<endl;
	}
return 0;
}

float suma (float a,float b){
	unsigned suma;
	suma=a+b;
return suma;
}

float resta (float a,float b){
	int resta;
	resta=a-b;
return resta;
}

float mul (float a,float b){
	int mul;
	mul=a*b;
return mul;
}

float div (float a,float b){
	float div;
	div=a/b;
return div;
}
