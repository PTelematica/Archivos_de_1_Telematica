#include <iostream>
#include <cstdlib>
using namespace std;

bool esPerfecto (int n);
int mcDivisor (int n1, int n2);

int main (){
	char op;
	int n1, n2, tmp;
	bool eCorrecta;
	
	cout<<"_.-._.-._.-._.-._.-._.-._.-OPERACIONES-._.-._.-._.-._.-._.-._.-._"<<endl;
	cout<<endl;
	cout<<"p: Calcula los n�meros perfectos en el rango dado por dos n�meros"<<endl;
	cout<<"m: Calcula el m�nimo com�n m�ltiplo de dos n�meros"<<endl;
	cout<<"d: Calcula el m�ximo com�n divisor de dos n�meros"<<endl;
	cout<<"+, *, -, /: Realiza las operaciones correspondientes con dos n�meros"<<endl;
	
	cout<<endl;
	cout<<"Introduzca una opci�n"<<endl;
	do{
		eCorrecta=true;
		cin>>op;
		cout<<"Introduzca los dos n�meros"<<endl;
		cin>>n1>>n2;
		cout<<endl;
		
		switch (op){
			case 'p':
				if (n2<n1){
					tmp=n1;
					n1=n2;
					n2=tmp;
				}
				for (int n=n1;n<=n2;n++){
					if (esPerfecto (n)){
						cout<<n<<"  ";
					}
				}
				break;
			case 'm':
				cout<<n1*n2/mcDivisor(n1,n2);
				break;
			case 'd':
				cout<<mcDivisor (n1,n2);
				break;
			case '+':
				cout<<n1+n2<<endl;
				break;
			case '*':
				cout<<n1*n2<<endl;
				break;
			case '-':
				cout<<n1-n2<<endl;
				break;
			case '/':
				cout<<n1/n2<<endl;
				break;
			default:
				cout<<"Elecci�n incorrecta"<<endl;
				cout<<"Vuelva a introducir una opci�n"<<endl;
				eCorrecta=false;
		}
	}while (!eCorrecta);
	
return 0;
}

bool esPerfecto (int n){
	bool perfecto;
	int s=0;
	
	for (int d=1;d<n;d++){
		if (n%d==0){
			s+=d;
		}
	}
	if (s==n){
		perfecto=true;
	}else{
		perfecto=false;
	}
return perfecto;
}


int mcDivisor (int n1, int n2){
	int tmp;
	
	while (n1!=n2){
		if (n2>n1){
			tmp=n1;
			n1=n2;
			n2=tmp;
		}
		
		if (n1>n2){
			n1-=n2;
		}
	}
return n1;
}
