#include <iostream>
using namespace std;

float potencia (float a,int b);

int main (){
	float a;
	int b;
	
	cout<<"Introduzca la base y el exponente"<<endl;
	cin>>a>>b;
	
	cout<<potencia (a,b)<<endl;
return 0;
}

float potencia (float a,int b){
	float pot=1;
	
	if (b<0){
		b=-b;
		a=1/a;
	}
	
	for (int i=0;i<b;i++){
		pot=pot*a;
	}
return pot;
}
