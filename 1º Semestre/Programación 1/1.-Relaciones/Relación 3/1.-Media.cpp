#include <iostream>
using namespace std;

const int TAM_CLASE=20;

typedef float Alumnos[TAM_CLASE];

int main (){
	Alumnos altura;
	float suma=0,media;
	int bajos=0,altos=0;
	
	cout<<"Introduzca las alturas de los "<<TAM_CLASE<<" alumnos"<<endl;
	
	for (int i=0;i<TAM_CLASE;i++){
		cout<<i+1<<": ";
		cin>>altura[i];
		suma+=altura[i];
	}
	
	media=suma/TAM_CLASE;
	
	for (int i=0;i<TAM_CLASE;i++){
		if (altura[i]<media){
			bajos++;
		}else{
			if (altura[i]>media){
				altos++;
			}
		}
	}
	
	cout<<"La media es "<<media<<". Hay "<<altos<<" alumnos m�s altos que la media, y "<<bajos<<" m�s bajos"<<endl;
return 0;
}
