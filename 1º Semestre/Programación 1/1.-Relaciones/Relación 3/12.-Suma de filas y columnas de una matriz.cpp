#include <iostream>
using namespace std;

const int TAM1X=8, TAM2X=9, TAM1Y=6, TAM2Y=7;

typedef int Matriz[TAM1Y][TAM1X];

struct Matriz_con_suma{
	Matriz matrizInicial={1,2,3,4,5,6,7,1,
						  1,2,3,4,5,6,7,2,
						  1,2,3,4,5,6,7,3,
						  1,2,3,4,5,6,7,4,
						  1,2,3,4,5,6,7,5,
						  1,2,3,4,5,6,7,6};
	Matriz matrizSuma;
};

int suma_fila (const Matriz_con_suma &matriz_con_suma, int i);

void inicializar_matriz (Matriz_con_suma &matriz_con_suma);

int main (){
	Matriz_con_suma matriz_con_suma;
	int suma;
	
	inicializar_matriz (matriz_con_suma);
	
	for (int o=0;o<TAM1X;o++){
		for (int i=0;i<TAM1Y;i++){
			suma=suma_fila (matriz_con_suma,i);
		}
		
		matriz_con_suma.matrizSuma[o][8]=suma;
	}
	
	for (int i=0;i<TAM2Y;i++){
		for (int o=0;o<TAM2X;o++){
			cout<<matriz_con_suma.matrizSuma[i][o]<<"   ";
		}
		cout<<endl;
	}
return 0;
}

int suma_fila (const Matriz_con_suma &matriz_con_suma, int i){
	int suma=0;
	for (int o=0;o<TAM1X;o++){
		suma+=matriz_con_suma.matrizInicial[i][o];
	}
return suma;
}

void inicializar_matriz (Matriz_con_suma &matriz_con_suma){
	for (int i=0;i<TAM1Y;i++){
		for (int o=0;o<TAM1X;o++){
			matriz_con_suma.matrizSuma[i][o]=matriz_con_suma.matrizInicial[i][o];
		}
	}
}
