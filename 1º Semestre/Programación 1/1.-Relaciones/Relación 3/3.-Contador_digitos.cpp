#include <iostream>
using namespace std;

const int TAM=10;

typedef int dig[TAM];

int main (){
	unsigned long n;
	dig digitos;
	
	cout<<"Introduzca un n�mero"<<endl;
	cin>>n;
	
	for (int i=0;i<TAM;i++){
		digitos[i]=0;
	}
	
	while (n!=0){
		digitos[n%10]++;
		n/=10;
	}
	
	for (int i=0;i<TAM;i++){
		if (digitos [i]!=0){
			cout<<"Hay "<<digitos[i]<<" digitos "<<i<<endl;
		}
	}
return 0;
}
