#include <iostream>
using namespace std;

const int TAM=1000000;

typedef bool Vec [TAM];

void rellenar_vector (Vec &vec);

int main (){
	int vueltas=2,ult=1,i=0;
	Vec nPrimos;
	
	rellenar_vector (nPrimos);
	
	do {
		while ((i<TAM)&&(i+vueltas<TAM)){
			nPrimos[i+vueltas]=false;
			i+=vueltas;
		}
		
		i=vueltas-1;
		vueltas++;
	}while (vueltas<=1+TAM);
	
	for (int i=0;i<TAM-1;i++){
		if (nPrimos[i]){
			cout<<i+2<<" ";
		}
		ult++;
	}
return 0;
}

void rellenar_vector (Vec &vec){
	for (int i=0;i<TAM;i++){
		vec[i]=true;
	}
}
