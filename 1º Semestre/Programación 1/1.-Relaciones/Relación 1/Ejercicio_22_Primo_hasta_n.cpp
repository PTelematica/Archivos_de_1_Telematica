#include <iostream>

using namespace std;

int main (){
	unsigned n, o;
	bool esPrimo;
	
	cout<<"Introduzca el l�mite"<<endl;
	cin>>n;
	
	for (unsigned i=2;i<=n;i++){
		esPrimo=true;
		
		o=2;
		while ((o<i)&&(esPrimo==true)){
			if (i%o==0){
				esPrimo=false;
			}
			o++;
		}
		if (esPrimo){
			cout<<i<<"   ";
		}
	}
	
	cout<<endl;
return 0;
}
