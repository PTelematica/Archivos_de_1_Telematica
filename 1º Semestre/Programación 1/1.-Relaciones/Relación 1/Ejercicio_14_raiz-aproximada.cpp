#include <iostream>
using namespace std;

int main (){
	int n,i=0;
	bool exacta=false;
	
	cout<<"Introduzca un n�mero entero"<<endl;
	cin>>n;
	
	do {
		i++;
		
		if (i*i==n){
			cout<<"La ra�z cuadrada de "<<n<<" es "<<i<<endl;
			
			exacta=true;
		}
	}while (i*i<=n);
	
	i=1;
	
	if (!exacta){
		do {
			i++;
			
			if (i*i>n){
				cout<<"El n�mero entero inferior m�s pr�ximo a la ra�z cuadrada de "<<n<<" es "<<i-1<<endl;
			}
		}while (i*i<n);
	}
return 0;
}
