#include <iostream>
using namespace std;

int main (){
	int num,m=0;
	
	cout<<"Introduzca el n�mero en el sistema decimal entre 1 y 3999"<<endl;
	cin>>num;
	
	do {
		while((num<0)||(num>=4000)) {
			cout<<"ERROR. El n�mero no es v�lido"<<endl;
			cin>>num;
		}
		if (num!=0){
			while (num/1000!=0){
			m++;
			num=num-1000;
				if (m>0){
					cout<<"M";
				}
			}
			m=0;
			while (num/900!=0){
				m++;
				num=num-900;
				if (m>0){
					cout<<"CM";
				}
			}
			m=0;
			while (num/800!=0){
				m++;
				num=num-800;
				if (m>0){
					cout<<"DCCC";
				}
			}
			m=0;
			while (num/700!=0){
				m++;
				num=num-700;
				if (m>0){
					cout<<"DCC";
				}
			}
			m=0;
			while (num/600!=0){
				m++;
				num=num-600;
				if (m>0){
					cout<<"DC";
				}
			}
			m=0;
			while (num/500!=0){
				m++;
				num=num-500;
				if (m>0){
					cout<<"D";
				}
			}
			m=0;
			while (num/400!=0){
				m++;
				num=num-400;
				if (m>0){
					cout<<"CD";
				}
			}
			m=0;
			while (num/100!=0){
				m++;
				num=num-100;
				if (m>0){
					cout<<"C";
				}
			}
			m=0;
			while (num/90!=0){
				m++;
				num=num-90;
				if (m>0){
					cout<<"XC";
				}
			}
			while (num/80!=0){
				m++;
				num=num-80;
				if (m>0){
					cout<<"LXXX";
				}
			}
			m=0;
			while (num/70!=0){
				m++;
				num=num-70;
				if (m>0){
					cout<<"LXX";
				}
			}
			m=0;
			while (num/60!=0){
				m++;
				num=num-60;
				if (m>0){
					cout<<"LX";
				}
			}
			m=0;
			while (num/50!=0){
				m++;
				num=num-50;
				if (m>0){
					cout<<"L";
				}
			}
			m=0;
			while (num/40!=0){
				m++;
				num=num-40;
				if (m>0){
					cout<<"XL";
				}
			}
			m=0;
			while (num/10!=0){
				m++;
				num=num-10;
				if (m>0){
					cout<<"X";
				}
			}
			m=0;
			while (num/9!=0){
				m++;
				num=num-9;
				if (m>0){
					cout<<"IX";
				}
			}
			m=0;
			while (num/8!=0){
				m++;
				num=num-8;
				if (m>0){
					cout<<"VIII";
				}
			}
			m=0;
			while (num/7!=0){
				m++;
				num=num-7;
				if (m>0){
					cout<<"XII";
				}
			}
			m=0;
			while (num/6!=0){
				m++;
				num=num-6;
				if (m>0){
					cout<<"VI";
				}
			}
			m=0;
			while (num/5!=0){
				m++;
				num=num-5;
				if (m>0){
					cout<<"V";
				}
			}
			m=0;
			while (num/4!=0){
				m++;
				num=num-4;
				if (m>0){
					cout<<"IV";
				}
			}
			m=0;
			while ((num==3)||(num==2)||(num==1)){
				cout<<"I";
				num=num-1;
			}
		}
		
		if (num!=0){
			cout<<endl;
			cout<<"Introduzca otro n�mero"<<endl;
			cin>>num;
		}
	} while(num!=0);
	cout<<endl;
	cout<<"Hasta luego"<<endl;
return 0;
}
