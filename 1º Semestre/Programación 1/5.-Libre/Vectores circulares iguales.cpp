#include <iostream>
using namespace std;

const int TAM=5;

typedef int Vector [TAM];

bool igualdad(const Vector &vector1, const Vector &vector2, unsigned i, unsigned o);

int main (){
	Vector vector1,vector2;
	bool circular;
	unsigned i=0,o=0;
	
	cout<<"Introduzca los dos vectores de 5 componentes"<<endl;
	for (int a=0;a<TAM;a++){
		cin>>vector1[a];
	}
	
	for (int a=0;a<TAM;a++){
		cin>>vector2[a];
	}
	//////////////////////////////////////////////////////////////////////
	while ((i<TAM)&&(!circular)){
		while ((o<TAM)&&(!circular)){
			if (vector1[i]==vector2[o]){
				circular=igualdad(vector1,vector2,i,o);
			}
			o++;
		}
		i++;
	}
	///////////////////////////////////////////////////////////////////////
	if (circular){
		cout<<"Los vectores introducidos son circularmente iguales"<<endl;
	}else{
		cout<<"Los vectores introducidos no son circularmente iguales"<<endl;
	}
return 0;
}

bool igualdad(const Vector &vector1, const Vector &vector2, unsigned i, unsigned o){
	int c=0;
	bool igualdad=false;
	
	while ((c<TAM)&&(vector1[i]==vector2[o])){
		i=(i+1)%TAM;
		o=(o+1)%TAM;
		c++;
	}
	
	if (c==TAM){
		igualdad=true;
	}
return igualdad;
}
