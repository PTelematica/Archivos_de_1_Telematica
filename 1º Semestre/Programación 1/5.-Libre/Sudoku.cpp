#include <iostream>

using namespace std;

const int TAM = 9;
const int PRED=9;

struct TSudoku{
	bool predeterminado;
	int num;
};

typedef TSudoku TTabla [TAM][TAM];

void inicializar_tabla (TTabla &sudoku);
void aleatorio (TTabla &sudoku);
void comprobacion_fila ();

int main (){
	TTabla sudoku;
	
	inicializar_tabla (sudoku);
	
	for (int i=0;i<PRED;i++){
		aleatorio (sudoku);
	}
return 0;
}

void inicializar_tabla (TTabla &sudoku){
	for (int i=0;i<TAM;i++){
		for (int o=0;o<TAM;o++){
			sudoku[i][o].predeterminado=false;
			sudoku[i][o].num=0;
		}
	}
}

void aleatorio (TTabla &sudoku){
	int i, o, n;
	
	do {
		i=random()%9;
		o=random()%9;
		n=1+random()%10
	}while (sudoku[i][o].predeterminado==true);
	
	sudoku[i][o].num=n;
	sudoku[i][o].predeterminado==true;
}
