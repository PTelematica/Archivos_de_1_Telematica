#include <iostream>
#include <string>

using namespace std;

int main(){
	string nombre;
	int espacios=0;
	
	cout<<"Introduzca su nombre"<<endl;
	
	getline (cin,nombre);
	
	for (unsigned i=0;i<nombre.size();i++){
		if (nombre[i]==' '){
			espacios++;
		}
	}
	
	cout<<"Su nombre tiene "<<nombre.size()-espacios<<" letras"<<endl;
return 0;
}
