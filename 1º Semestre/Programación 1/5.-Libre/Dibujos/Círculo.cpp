#include <iostream>
using namespace std;

const int TAM=100;

typedef bool TTabla [TAM][TAM];

struct TCirculo{
	int t;
	TTabla tabla;
};

void inicializar_tabla (TCirculo &circulo);

int main (){
	TCirculo circulo;
	int n;
	
	/*PETICI�N DE N�MERO
	cout<<"Introduzca un n�mero"<<endl;
	cin>>n;
	*/
	
	n=10;
	circulo.t=n*2-1;
	
	inicializar_tabla (circulo);
	
	for (int i=0;i<circulo.t;i++){//EJE Y
		for (int o=n-n/3;o<(n-1)+n-n/3;o++){
			circulo.tabla[i][o]=true;
			circulo.tabla[o][i]=true;
		}
	}
	
	for (int i=0;i<circulo.t;i++){//MOSTRAR LA TABLA
		cout<<i<<". ";
		for (int o=0;o<circulo.t;o++){
			cout<<circulo.tabla[i][o]<<"  ";
		}
		cout<<endl;
	}
	cout<<"   ";
	for (int i=0;i<circulo.t;i++){
		cout<<i<<"  ";
	}
	cout<<endl;
return 0;
}
void inicializar_tabla (TCirculo &circulo){
	for (int i=0;i<circulo.t;i++){
		for (int o=0;o<circulo.t;o++){
			circulo.tabla[i][o]=false;
		}
	}
}
