#include <iostream>

using namespace std;

int main (){
	int h;
	
	cout<<"Introduzca la altura"<<endl;
	cin>>h;
	
	for (int y=0;y<h;y++){//Altura de la parte superior
		for (int bi=0;bi<y;bi++){//Espacios blancos para brazo izquierdo
			cout<<" ";
		}
		
		if (y!=h-1){//Brazo izquierdo
			cout<<"*";
		}
		
		for (int ep=0;ep<(h-y-1)*2-1;ep++){//Espacio pir�mide
			cout<<" ";
		}
		
		for (int p=0;p<=y*2;p++){//Pir�mide
			cout<<"*";
		}
		
		for (int ep=0;ep<(h-y-1)*2-1;ep++){//Espacio pir�mide-brazo derecho
			cout<<" ";
		}
		
		if (y!=h-1){//Brazo derecho
			cout<<"*";
		}
		
		cout<<endl;
	}
	
	for (int y=0;y<h;y++){//Altura de la parte inferior
		//Espacios piramide invertida
		for (int e=0;e<h;e++){
			cout<<" ";
		}
		
		for (int epi=0;epi<y;epi++){
			cout<<" ";
		}
		//////////////////////////////
		
		for (int pi=0;pi<(h-y)*2-3;pi++){//Pir�mide invertida
			cout<<"*";
		}
		cout<<endl;
	}
return 0;
}
