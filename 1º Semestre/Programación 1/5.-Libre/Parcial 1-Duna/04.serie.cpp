//Pablo L�pez Sede�o
#include <iostream>
using namespace std;

float division (int i);
float potencia (int i);
float factorial (int i);

int main (){
	float n,sum,div;
	
	cout<<"Introduzca el valor l�mite"<<endl;
	cin>>n;
	
	for (int i=1;i<=n;i++){
	div=division(i);
	
	sum+=div;
	}
	cout<<"El resultado de la serie terminada en "<<n<<" es "<<sum<<endl;
return 0;
}

float division (int i){
	float div,pot,fact;
	
	pot=potencia (i);
	fact=factorial (i);
	
	div=pot/fact;
return div;
}

float potencia (int i){
	float pot=1;
	
	for (int o=0;o<i;o++){
		pot*=i;
	}
return pot;
}

float factorial (int i){
	float fact=1;
	
	for (int o=1;o<=i;o++){
		fact*=o;
	}
return fact;
}
