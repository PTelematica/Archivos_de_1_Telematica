#include <iostream>
using namespace std;

const int TAM=10;

typedef int Vec[TAM];

void rellenar_vector (Vec &vec);

int main (){
	Vec vec;
	
	rellenar_vector (vec);
	
	for (int i=0;i<TAM-1;i++){
		cout<<vec[i]<<", ";
	}
	cout<<vec[TAM-1]<<endl;
return 0;
}

void rellenar_vector (Vec &vec){
	for (int i=0;i<TAM;i++){
		vec[i]=1+rand()%51;
	}
}
