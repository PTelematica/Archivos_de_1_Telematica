#include <iostream>
using namespace std;

void sumaparesimpares(int &sumapares,int &sumaimpares);

int main (){
	int sumapares=0,sumaimpares=0;
	
	sumaparesimpares(sumapares,sumaimpares);
	cout<<"La suma de los n�meros pares es "<<sumapares<<", y la suma de los n�meros impares es "<<sumaimpares<<endl;
return 0;
}

void sumaparesimpares(int &sumapares,int &sumaimpares){
	int n;
	
	cout<<"Introduzca n�meros hasta 0"<<endl;
	cin>>n;
	
	while (n!=0){
		if (n%2==0){
			sumapares+=n;
		}else{
			sumaimpares+=n;
		}
		
		cin>>n;
	}
}
