#include <iostream>
#include <string>
using namespace std;

const int TAM=20;

typedef int TTabla [TAM][TAM];
typedef string TDatos [TAM];

struct TMatriz {
	int tx, ty;
	TTabla tabla;
};

void inicializar_matriz (TMatriz &matriz);
void de_string_a_TMatriz (TMatriz &matriz, const TDatos &datosMatriz);

int main (){
	TMatriz matriz;
	TDatos datosMatriz;
	int d=0;
	
	inicializar_matriz (matriz);
	
	cout<<"Introduzca los datos de su matriz"<<endl;
	do{
		getline(cin,datosMatriz[d]);
		d++;
	}while ((d<TAM)&&(datosMatriz[d-1]!=""));
	
	de_string_a_TMatriz (matriz, datosMatriz);
	
	for (int i=0;i<matriz.ty;i++){
		for (int o=0;o<matriz.tx;o++){
			cout<<matriz.tabla[i][o];
		}
		cout<<endl;
	}
return 0;
}

void inicializar_matriz (TMatriz &matriz){
	for (int i=0;i<TAM;i++){
		for (int o=0;o<TAM;o++){
			matriz.tabla[i][o]=0;
		}
	}
}

void de_string_a_TMatriz (TMatriz &matriz, const TDatos &datosMatriz){
	string tmp="";
	int elemento, x=0, y=0;
	
	while ((y<TAM)&&(datosMatriz[y]!="")){
		for (int i=0;i<datosMatriz[y].size();i++){
			if ((datosMatriz[y][i]!=" ")||(datosMatriz[y][i]!="")){
				tmp*=datosMatriz[y][i];
			}else{
				elemento=tmp;
				matriz.tabla[y][x]=elemento;
				x++;
			}
		}
		y++;
	}
	matriz.tx=x;
	matriz.ty=y;
}
