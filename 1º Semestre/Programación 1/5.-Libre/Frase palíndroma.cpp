#include <iostream>
#include <string>

using namespace std;

string sinEspacios (string cad);
string alReves (string cad);

int main (){
	string cad, cadSin, cadReves;
	
	cout<<"Introduzca una frase"<<endl;
	getline (cin,cad);
	
	cadSin=sinEspacios (cad);
	cadReves=alReves (cadSin);
	
	
	
	if (cadSin==cadReves){
		cout<<"La frase introducida es palíndroma"<<endl;
	}else{
		cout<<"La frase introducida no es palíndroma"<<endl;
	}
return 0;
}

string sinEspacios (string cad){
	string cadAux="";
	
	for (unsigned i=0;i<cad.size();i++){
		if (cad[i]!=' '){
			cadAux+=cad[i];
		}
	}
return cadAux;
}

string alReves (string cad){
	string cadReves="";
	
	for (unsigned i=0;i<cad.size();i++){
		cadReves+=cad[cad.size()-1-i];
	}
return cadReves;
}
