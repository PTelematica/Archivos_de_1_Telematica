#include <iostream>
using namespace std;

int mcd(int a,int b);

int main (){
	int a,b,mcm;
	
	cout<<"Introduzca dos n�meros"<<endl;
	cin>>a>>b;
	
	mcm=a*b/mcd(a,b);
	
	cout<<"El m�nimo com�n m�ltiplo es "<<mcm;
return 0;
}

int mcd(int a,int b){
	int c,mcd;
	
	do{
		if (b>a){
			c=a;
			a=b;
			b=c;
		}
		
		if (a>b){
			a=a-b;
		}
		
		if (a==b){
			mcd=a;
		}
	}while (a!=b);
return mcd;
}
