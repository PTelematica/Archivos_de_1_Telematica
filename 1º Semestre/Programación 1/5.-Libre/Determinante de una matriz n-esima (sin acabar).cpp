#include <iostream>
using namespace std;

const int TAM=50;

typedef float TDatos [TAM][TAM];

struct TMatriz{
	unsigned orden;
	TDatos datos;
	};
	
float det (const TMatriz &matriz, TMatriz &menor, int &signo);
void menorComplementario (const TMatriz &matriz, TMatriz &menor, unsigned i, unsigned j);

int main (){
	TMatriz matriz, menor;
	float determinante;
	int signo=-1;
	
	cout<<"Introduzca el orden de la matriz (cuadrada) para calcular su determinante"<<endl;
	cin>>matriz.orden;
	menor.orden=matriz.orden-1;
	
	cout<<"Introduzca los datos de la matriz"<<endl;
	
	for (unsigned i=0;i<matriz.orden;i++){//Pedir matriz.datos
		for (unsigned j=0;j<matriz.orden;j++){
			cin>>matriz.datos[i][j];
		}
	}
	
	for (unsigned i=0;i<matriz.orden;i++){//Mostrar matriz.datos
		for (unsigned j=0;j<matriz.orden;j++){
			cout<<matriz.datos[i][j]<<"  ";
		}
		cout<<endl;
	}
	
	
	
	determinante=det (matriz, menor, signo);//Iniciar determinante
	
	cout<<"El determinante de la matriz introducida es "<<determinante<<endl;
return 0;
}

float det (const TMatriz &matriz, TMatriz &menor, int &signo){
	float determinante;
	signo=-signo;
	
	if (menor.orden<3){
		for (unsigned i=0;i<matriz.orden;i++){
			for (unsigned j=0;j<matriz.orden;j++){
				menorComplementario (matriz, menor, i, j);
				determinante=signo*matriz.datos[i][j]*det(menor, menor, signo)+determinante;
			}
		}
	}else{
		determinante=menor.datos[1][1]*menor.datos[2][2]-menor.datos[2][1]*menor.datos[1][2];
	}
return determinante;
}

void menorComplementario (const TMatriz &matriz, TMatriz &menor, unsigned i, unsigned j){
	unsigned x1, y1=0;
	
	for (unsigned y=0;y<menor.orden;y++){
		x1=0;
		for (unsigned x=0;x<menor.orden;x++){
			if (y==i){
				y1++;
			}
			if (x==j){
				x1=(x1+1)%menor.orden;
			}
			menor.datos[y][x]=matriz.datos[y1][x1];
			x1++;
		}
		y1++;
	}
	
}
