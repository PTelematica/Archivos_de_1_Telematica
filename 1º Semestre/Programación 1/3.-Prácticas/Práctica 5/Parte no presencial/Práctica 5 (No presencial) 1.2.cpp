#include <iostream>
using namespace std;

const int XMAX=64;
const int YMAX=48;

typedef float TDatos[YMAX][XMAX];

struct TMatriz {
	unsigned x,y;
	TDatos datos;
};

void copiarMatriz (const TMatriz &matrizInicial, TDatos &matrizCopia);
void extraer (const TMatriz &matrizInicial, TMatriz &matrizExtraida, int radio, int x, int y);
void rellenar_matriz (int radio, TMatriz &matrizExtraida, const TMatriz &matrizInicial, int cx, int cy, int xp, int yp);

int main (){
	TMatriz matrizInicial,matrizExtraida;
	TDatos matrizCopia;
	int radio,x,y;//radio de la matrizExtraida
	/*//Pedir datos matriz inicial
	cout<<"Introduzca el tama�o de la matriz"<<endl;
	cout<<"X: ";
	cin>>matrizInicial.x;
	cout<<"Y: ";
	cin>>matrizInicial.y;
	cout<<"Introduzca los componentes de la matriz"<<endl;
	for (unsigned i=0;i<matrizInicial.y;i++){//Pedir matrizInicial
	*/
	//Datos predefinidos matriz inicial
	unsigned a=0;
	matrizInicial.x=5;
	matrizInicial.y=6;
	for (unsigned i=0;i<matrizInicial.y;i++){
	
		for (unsigned o=0;o<matrizInicial.x;o++){
			matrizInicial.datos[i][o]=a;//(i+o)%matrizInicial.x+1;
			a++;
		}
	}
	
	//Matriz copia
	copiarMatriz(matrizInicial,matrizCopia);
	
	for (unsigned i=0;i<matrizInicial.y;i++){//Motrar matrizCopia
	
	
		for (unsigned o=0;o<matrizInicial.x;o++){
			cout<<matrizCopia[i][o]<<"   ";
		}
		cout<<endl;
	}
	
	//Matriz extraida
	cout<<"Introduzca las coordenadas del centro de la submatriz"<<endl;
	cout<<"X: ";
	cin>>x;
	cout<<"Y: ";
	cin>>y;
	cout<<"Introduzca el radio de la matriz submatriz"<<endl;
	cin>>radio;
	
	//Extraer matrizExtraida
	extraer(matrizInicial, matrizExtraida, radio, x, y);
	
	cout<<endl;
	
	for (unsigned i=0;i<matrizExtraida.y;i++){//mostrar matrizExtraida
		for (unsigned o=0;o<matrizExtraida.x;o++){
			cout<<matrizExtraida.datos[i][o]<<"   ";
		}
		cout<<endl;
	}
return 0;
}

void copiarMatriz(const TMatriz &matrizInicial, TDatos &matrizCopia){
	for (unsigned i=0;i<matrizInicial.y;i++){
		for (unsigned o=0;o<matrizInicial.x;o++){
			matrizCopia[i][o]=matrizInicial.datos[i][o];
		}
	}
}

void extraer(const TMatriz &matrizInicial, TMatriz &matrizExtraida, int radio, int x, int y){
	int x0,y0,cx=0,cy=0,xp,yp;
	
	x0=matrizInicial.x;
	y0=matrizInicial.y;
	
	if ((y<=y0)&&(y>=0)&&(x<=x0)&&(x>=0)){
		
		while ((cx<radio*2+1)&&(cy<radio*2+1)){
			if (y-(1+radio)<0){
				yp=y-(y-(1+radio));
			}
			
			if (x-(1+radio)<0){
				xp=x-(x-(1+radio));
			}
			
			if (y+radio>y0){
				yp=y-(radio-(y0-y));
			}
			
			if (x+radio>x0){
				xp=x-(radio-(x0-x));
			}
			
			rellenar_matriz (radio, matrizExtraida, matrizInicial, cx, cy, xp, yp);
			
			cx++;
			cy++;
			x++;
			y++;
		}
		matrizExtraida.x=radio*2+1;
		matrizExtraida.y=radio*2+1;
	}
}

void rellenar_matriz (int radio, TMatriz &matrizExtraida, const TMatriz &matrizInicial, int cx, int cy, int xp, int yp){
	int a=0,e=0;
	
	matrizExtraida.datos[a][e]=matrizInicial.datos[cy+(yp-radio)][cx+(xp-radio)];
	e++;
	a++;
}
