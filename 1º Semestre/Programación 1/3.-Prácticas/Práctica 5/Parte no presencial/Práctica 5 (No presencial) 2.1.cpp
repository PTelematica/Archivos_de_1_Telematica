#include <iostream>
using namespace std;

const int XMAX=64;
const int YMAX=48;

typedef float TDatos[YMAX][XMAX];

struct TMatriz {
	unsigned x,y;
	TDatos datos;
};

void copiarMatriz(const TMatriz &matrizInicial, TDatos &matrizCopia);
void extraer(const TMatriz &matrizInicial, TMatriz &matrizExtraida, int radio, int x, int y);
void matrizUniforme (int t, TMatriz &matrizUni);

int main (){
	TMatriz matrizInicial, matrizExtraida, matrizUni;
	TDatos matrizCopia;
	int radio, x, y, t;//radio de la matrizExtraida
	//Pedir datos matriz inicial
	cout<<"Introduzca el tama�o de la matriz"<<endl;
	cout<<"X: ";
	cin>>matrizInicial.x;
	cout<<"Y: ";
	cin>>matrizInicial.y;
	cout<<"Introduzca los componentes de la matriz"<<endl;
	for (unsigned i=0;i<matrizInicial.y;i++){//Pedir matrizInicial
	
		for (unsigned o=0;o<matrizInicial.x;o++){
			cin>>matrizInicial.datos[i][o];
		}
	}
	
	/*
	//Datos predefinidos matriz inicial
	unsigned a=0;
	matrizInicial.x=5;
	matrizInicial.y=6;
	for (unsigned i=0;i<matrizInicial.y;i++){
		for (unsigned o=0;o<matrizInicial.x;o++){
			matrizInicial.datos[i][o]=a;//(i+o)%matrizInicial.x+1;
			a++;
		}
	}
	*/
	
	//Matriz copia
	copiarMatriz(matrizInicial,matrizCopia);
	
	for (unsigned i=0;i<matrizInicial.y;i++){//Motrar matrizCopia
	
		for (unsigned o=0;o<matrizInicial.x;o++){
			cout<<matrizCopia[i][o]<<"   ";
		}
		cout<<endl;
	}
	
	//Matriz extraida
	cout<<"Introduzca las coordenadas del centro de la submatriz"<<endl;
	cout<<"X: ";
	cin>>x;
	cout<<"Y: ";
	cin>>y;
	cout<<"Introduzca el radio de la matriz submatriz"<<endl;
	cin>>radio;
	
	//Extraer matrizExtraida
	extraer(matrizInicial, matrizExtraida, radio, x, y);
	
	cout<<endl;
	
	for (unsigned i=0;i<matrizExtraida.y;i++){//mostrar matrizExtraida
		for (unsigned o=0;o<matrizExtraida.x;o++){
			cout<<matrizExtraida.datos[i][o]<<"   ";
		}
		cout<<endl;
	}
	
	cout<<"Introduzca un n�mero impar"<<endl;
	cin>>t;
	
	while (t%2==0){
		cout<<"ERROR"<<endl;
		cout<<"Introduzca un n�mero impar"<<endl;
		cin>>t;
	}
	
	matrizUniforme (t, matrizUni);
	
	for (int i=0;i<t;i++){
		for (int o=0;o<t;o++){
			cout<<matrizUni.datos[i][o]<<"  ";
		}
		cout<<endl;
	}
return 0;
}

void copiarMatriz(const TMatriz &matrizInicial, TDatos &matrizCopia){
	for (unsigned i=0;i<matrizInicial.y;i++){
		for (unsigned o=0;o<matrizInicial.x;o++){
			matrizCopia[i][o]=matrizInicial.datos[i][o];
		}
	}
}

void extraer(const TMatriz &matrizInicial, TMatriz &matrizExtraida, int radio, int x, int y){
	int a=0,e,x0,y0,ch=0;
	
	matrizExtraida.x=radio*2+1;
	matrizExtraida.y=radio*2+1;
	
	x0=matrizInicial.x;
	y0=matrizInicial.y;
	
	if ((radio*2+1>y0)||(radio*2+1>x0)||(radio<0)){
		cout<<"El radio introducido no es adecuado"<<endl;
	}else{
		if (y-(1+radio)<0){
			//y=y-(y-(1+radio));
			for (int i=0;i<(1+radio)-y;i++){
				e=0;
				for (int o=x-(1+radio);o<x+radio;o++){
					matrizExtraida.datos[a][e]=matrizInicial.datos[i][o];
					e++;
				}
				a++;
			}
		}
		ch=a-1;
		/*
		if (x-(1+radio)<0){
			//x=x-(x-(1+radio));
			for (int ;;){
				for (int ;;){
				
				}
			}
		}
		
		if (y+radio>y0){
			//y=y-(radio-(y0-y));
			for (int ;;){
				for (int ;;){
				
				}
			}
		}
		
		if (x+radio>x0){
			//x=x-(radio-(x0-x));
			for (int ;;){
				for (int ;;){
				
				}
			}
		}
		*/
		a=0;
		for (int i=y-(1+radio);i<y+radio;i++){
			e=0;
			for (int o=x-(1+radio);o<x+radio;o++){
				matrizExtraida.datos[a+ch][e]=matrizInicial.datos[i][o];
				e++;
			}
			a++;
		}
	}
}
void matrizUniforme (int t, TMatriz &matrizUni){
	float n=t*t-1;
	
	for (int i=0;i<t;i++){
		for (int o=0;o<t;o++){
			if ((i==o)&&(o==t/2)){
				matrizUni.datos[i][o]=1;
			}else{
				matrizUni.datos[i][o]=-1/n;
			}
		}
	}
}
