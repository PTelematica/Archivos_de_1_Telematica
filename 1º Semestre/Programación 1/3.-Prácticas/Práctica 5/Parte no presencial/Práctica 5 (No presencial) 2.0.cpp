#include <iostream>
using namespace std;

const int XMAX=64;
const int YMAX=48;

typedef float TDatos[YMAX][XMAX];

struct TMatriz{
	unsigned ancho, alto;
	TDatos datos;
};

void copiarMatriz(const TMatriz &matriz, TMatriz &matrizCopia);
void extraerMatriz(const TMatriz &matriz, TMatriz &matrizExtraida, unsigned x, unsigned y, unsigned radio);

int main (){
	TMatriz matriz, matrizCopia, matrizExtraida;
	unsigned x,y,radio;
	/*
	cout<<"X: ";
	cin>>matriz.ancho;
	cout<<"Y: ";
	cin>>matriz.alto;
	*/
	matriz.ancho=6;
	matriz.alto=6;
	
	for (unsigned i=0;i<matriz.alto;i++){
		for (unsigned o=0;o<matriz.ancho;o++){
			cin>>matriz.datos[i][o];
		}
	}
	
	copiarMatriz(matriz, matrizCopia);
	
	for (unsigned i=0;i<matriz.alto;i++){
		for (unsigned o=0;o<matriz.ancho;o++){
			cout<<matrizCopia.datos[i][o]<<"  ";
		}
		cout<<endl;
	}
	cout<<endl;
	/*
	cout<<"Introduzca las coordenadas del centro de la matriz a extraer"<<endl;
	cout<<"X: ";
	cin>>x;
	cout<<"Y: ";
	cin>>y;
	cout<<"Introduzca el radio"<<endl;
	cin>>radio;
	*/
	x=4;
	y=1;
	radio=2;
	
	extraerMatriz(matriz, matrizExtraida, x, y, radio);
	
	for (unsigned i=0;i<matrizExtraida.alto;i++){
		for (unsigned o=0;o<matrizExtraida.ancho;o++){
			cout<<matrizExtraida.datos[i][o]<<"  ";
		}
		cout<<endl;
	}
return 0;
}

void copiarMatriz(const TMatriz &matriz, TMatriz &matrizCopia){
	for (unsigned i=0;i<matriz.alto;i++){
		for (unsigned o=0;o<matriz.ancho;o++){
			matrizCopia.datos[i][o]=matriz.datos[i][o];
		}
	}
}

void extraerMatriz(const TMatriz &matriz, TMatriz &matrizExtraida, unsigned x, unsigned y, unsigned radio){
	unsigned matEY, matEX;
	
	matrizExtraida.alto=radio*2+1;
	matrizExtraida.ancho=radio*2+1;
	
	if (y-radio<0){
		matEY=0;
		matEX=0;
		
		for (unsigned i=0;i<radio-y;i++){
			for (unsigned o=x-radio-1;o<x+radio;o++){
				matrizExtraida.datos[matEY][matEX]=matriz.datos[0][o];
				matEX=(matEX+1)%matrizExtraida.ancho;
			}
			matEY=(matEY+1)%matrizExtraida.alto;
		}
	}
	/*
	if (y+radio-1>matriz.alto){
		matEY=0;
		matEX=0;
		
		matrizExtraida.datos[][];
	}
	
	if (x-radio<0){
		matEY=0;
		matEX=0;
		
		matrizExtraida.datos[][];
	}
	
	if (x+radio-1>matriz.ancho){
		matEY=0;
		matEX=0;
		
		matrizExtraida.datos[][];
	}
	
	matEY=0;
	matEX=0;
	
	for (unsigned i=y-radio-1;i<y+radio;i++){
		for (unsigned o=x-radio-1;o<x+radio;o++){
			matrizExtraida.datos[matEY][matEX]=matriz.datos[i][o];
			matEX=(matEX+1)%matrizExtraida.ancho;
		}
		matEY=(matEY+1)%matrizExtraida.alto;
	}*/
}
