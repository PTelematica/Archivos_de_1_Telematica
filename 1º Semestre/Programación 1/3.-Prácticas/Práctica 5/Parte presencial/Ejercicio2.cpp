//Pablo López Sedeño
#include <iostream>
using namespace std;

const int XMAX=64;
const int YMAX=48;

typedef float TDatos[YMAX][XMAX];
typedef char TDatoschar[YMAX][XMAX];

struct TMatriz {
	unsigned x,y;
	TDatos datos;
};

struct TMatrizchar {
	unsigned x,y;
	TDatoschar datos;
};

float convolucion (const TMatriz &matriz1, const TMatriz &matriz2);
void copiarMatriz(const TMatriz &matrizInicial, TMatriz &matrizCopia);
void extraer(const TMatriz &matrizInicial, TMatriz &matrizExtraida, int x, int y);
void matrizUniforme (TMatriz &matrizUni);
bool iguales (TMatriz matriz1, TMatriz matriz2);

const int t=3;

int main(){
	TMatriz matrizInicial, matrizExtraida, matrizUni, matrizCopia;
	TMatrizchar matrizAsteriscos;
	
	//Pedir datos matriz inicial
	cout<<"Introduzca el tamaño de la matriz"<<endl;
	cout<<"X: ";
	cin>>matrizInicial.x;
	cout<<"Y: ";
	cin>>matrizInicial.y;
	cout<<"Introduzca los componentes de la matriz"<<endl;
	for (unsigned i=0;i<matrizInicial.y;i++){//Pedir matrizInicial
		for (unsigned o=0;o<matrizInicial.x;o++){
			cin>>matrizInicial.datos[i][o];
		}
	}
	
	copiarMatriz (matrizInicial, matrizCopia);
	
	matrizUniforme (matrizUni);//Matriz uniforme con t(cte)=3
	
	while (!iguales(matrizCopia, matrizInicial)){//Hasta que matrizCopia y matrizInicial sean iguales
		for (unsigned i=0;i<matrizInicial.y;i++){//Rellenar matrizInicial final
			for (unsigned o=0;o<matrizInicial.x;o++){
				extraer (matrizCopia, matrizExtraida, o, i);
				if ((convolucion(matrizExtraida, matrizUni)>=0)&&(convolucion(matrizExtraida, matrizUni)<=100)){
					matrizInicial.datos[i][o]=convolucion (matrizExtraida, matrizUni);
				}else{
					if (convolucion(matrizExtraida, matrizUni)<0){
						matrizInicial.datos[i][o]=0;
					}else{
						if (convolucion(matrizExtraida, matrizUni)>100){
							matrizInicial.datos[i][o]=100;
						}
					}
				}
			}
		}
	}
	
	for (unsigned i=0;i<matrizInicial.y;i++){//Rellenar matrizAsteriscos
		for (unsigned o=0;o<matrizInicial.x;o++){
			if (matrizInicial.datos[i][o]>0){
				matrizAsteriscos.datos[i][o]='*';
			}else{
				matrizAsteriscos.datos[i][o]='.';
			}
		}
	}
	matrizAsteriscos.x=matrizInicial.x;
	matrizAsteriscos.y=matrizInicial.y;
	
	for (unsigned i=0;i<matrizAsteriscos.y;i++){//Mostrar matrizAsteriscos
		for (unsigned o=0;o<matrizAsteriscos.x;o++){
			cout<<matrizAsteriscos.datos[i][o]<<"  ";
		}
		cout<<endl;
	}
	return 0;
}

float convolucion (const TMatriz &matriz1, const TMatriz &matriz2){
	float suma=0;
	
	for (unsigned i=0;i<matriz1.y;i++){
		for (unsigned o=0;o<matriz1.x;o++){
			suma=suma+(matriz1.datos[i][o]*matriz2.datos[i][o]);
		}
	}
	return suma;
}

void copiarMatriz(const TMatriz &matrizInicial, TMatriz &matrizCopia){
	for (unsigned i=0;i<matrizInicial.y;i++){
		for (unsigned o=0;o<matrizInicial.x;o++){
			matrizCopia.datos[i][o]=matrizInicial.datos[i][o];
		}
	}
	matrizCopia.x=matrizInicial.x;
	matrizCopia.y=matrizInicial.y;
}

void extraer(const TMatriz &matrizInicial, TMatriz &matrizExtraida, int x, int y){
	int a=0, e, x0, y0, radio=1;
	
	matrizExtraida.x=radio*2+1;
	matrizExtraida.y=radio*2+1;
	
	x0=matrizInicial.x;
	y0=matrizInicial.y;
	
	if (y-(1+radio)<0){
		y=y-(y-(1+radio));
	}
	
	if (x-(1+radio)<0){
		x=x-(x-(1+radio));
	}
	
	if (y+radio>y0){
		y=y-(radio-(y0-y));
	}
	
	if (x+radio>x0){
		x=x-(radio-(x0-x));
	}
	
	for (int i=y-(1+radio);i<y+radio;i++){
		e=0;
		for (int o=x-(1+radio);o<x+radio;o++){
			matrizExtraida.datos[a][e]=matrizInicial.datos[i][o];
			e++;
		}
		a++;
	}
}

void matrizUniforme (TMatriz &matrizUni){
	float n=t*t-1;
	
	for (int i=0;i<t;i++){
		for (int o=0;o<t;o++){
			if ((i==o)&&(o==t/2)){
				matrizUni.datos[i][o]=1;
			}else{
				matrizUni.datos[i][o]=-1/n;
			}
		}
	}
}

bool iguales (TMatriz matriz1, TMatriz matriz2){
	bool igual=true;
	unsigned i=0,o=0;
	
	while ((igual==true)&&(i<matriz1.y)){
		while ((igual==true)&&(o<matriz1.x)){
			if (matriz1.datos[i][o]!=matriz2.datos[i][o]){
				igual=!igual;
			}
			o++;
		}
		i++;
	}
	
	return igual;
}
