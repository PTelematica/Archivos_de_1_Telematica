//Pablo López Sedeño
#include <iostream>
using namespace std;

int numeros (int n);

int main (){
	int n1=1,n2;
	
	cout<<"Introduzca un número"<<endl;
	cin>>n2;
	
	cout<<n2;
	
	while (n1!=n2){
		n1=n2;
		n2=numeros (n2);
		cout<<n2;
	}
return 0;
}

int numeros (int n){
	int num=0,dig;
	
	while (n!=0){
		dig=n%10;
		n/=10;
		num+=dig;
	}
return num;
}
