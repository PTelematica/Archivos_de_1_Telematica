//Pablo L�pez Sede�o
#include <iostream>
using namespace std;

int main (){
	int n,inicio=1,suma;
	
	cout<<"Introduzca el n�mero l�mite para calcular su cubo"<<endl;
	cin>>n;
	
	for (int i=1;i<=n;i++){
		cout<<i<<"^3 = ";
		suma=0;
		
		for (int o=1;o<=i;o++){
			cout<<inicio;
			if (o<i){
				cout<<" + ";
			}else{
				cout<<" = ";
			}
			suma+=inicio;
			inicio+=2;
		}
		
		cout<<suma<<endl;
	}
return 0;
}
