//Pablo López Sedeño
#include <iostream>
using namespace std;

bool divPrimo(int n);

int main (){
	int n,suma=0;
	
	cout<<"Introduzca un número"<<endl;
	cin>>n;
	
	for (int i=2;i<n;i++){
		if ((n%i==0)&&(divPrimo(i))){
			suma+=i;
		}
	}
	
	cout<<"La suma de los divisores primos del número "<<n<<" es "<<suma<<endl;
return 0;
}

bool divPrimo(int n){
	bool prim=true;
	
	for (int i=2;i<n;i++){
		if (n%i==0){
			prim=false;
		}
	}
	
return prim;
}
