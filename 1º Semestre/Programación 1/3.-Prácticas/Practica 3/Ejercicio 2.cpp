//Pablo López Sedeño
#include <iostream>
using namespace std;

bool digIguales (int digA,int b);

int main (){
	int a,b,digA;
	
	cout<<"Introduzca dos números"<<endl;
	cin>>a>>b;
	
	while (a!=0){
		digA=a%10;
		a/=10;
		
		if (digIguales(digA,b)){
			cout<<digA<<endl;
		}
	}
return 0;
}

bool digIguales (int digA,int b){
	int digB;
	bool si=false;
	
	while (b!=0){
			digB=b%10;
			b/=10;
			
			if (digA==digB){
				si=true;
			}
		}
	
return si;
}
