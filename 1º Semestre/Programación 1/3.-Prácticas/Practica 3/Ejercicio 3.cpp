//Pablo López Sedeño
#include <iostream>
using namespace std;

int mcdivisor(int n1,int n2);

int main (){
	int n1,n2,mcd;
	
	cout<<"Introduzca números hasta 0"<<endl;
	cin>>n1;
	
	if (n1!=0){
		cin>>n2;
	}
	
	while (n2!=0){
		mcd=mcdivisor(n1,n2);
		
		cout<<mcd<<endl;
		
		n1=n2;
		cin>>n2;
	}
return 0;
}

int mcdivisor(int n1,int n2){
	int tmp,mcd;
	
	do{
		if (n1>n2){
			tmp=n2;
			n2=n1;
			n1=tmp;
		}
		
		if (n2>n1){
			n2=n2-n1;
		}
		
		if (n2==n1){
			mcd=n2;
		}
	}while (n2!=n1);
	
return mcd;
}
