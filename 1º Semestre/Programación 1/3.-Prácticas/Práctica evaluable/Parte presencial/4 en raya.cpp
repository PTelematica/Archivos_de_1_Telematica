#include <iostream>
#include <string>
using namespace std;

const int TX = 10;
const int TY = 10;

enum TColor { Blanco, Azul, Rojo };

typedef TColor TMatriz[TY][TX];
typedef string TArraycolor[3];

void rellenarMatriz (TMatriz &tablero);
void dibujarMatriz (const TMatriz &matriz);
bool turno (int &a);
void usoDeColumna (const TMatriz &matriz, const TColor &cuantos, int nColumna, int &diferentes);
void avanzar (int &fila, int &col, unsigned dir);
void celdasConsecutivas (unsigned &pasos, int fila, int col, unsigned dir, const TColor &color, const TMatriz &matriz, int &cont);
void colocarFicha (TMatriz &tablero, const TColor &jugador, int col);
void pedirColor (string color, TColor &colores, TArraycolor &eleccion);
bool cuatroEnRaya (const TMatriz &tablero, const TColor &jugador, int fila, int col);
bool gana (const TMatriz &tablero, const TColor &jugador);

int main (){
	TMatriz tablero;
	int a=1, col, dif;
	string jugador;
	TColor colores, blanco=Blanco;
	TArraycolor eleccion={"Blanco", "Azul", "Rojo"};
	
	rellenarMatriz (tablero);
	
	do{
		dibujarMatriz (tablero);
		switch (turno(a)){
			case 1:
				jugador="Rojo";
				break;
			case 0:
				jugador="Azul";
				break;
		}
		cout<<endl;
		cout<<"Turno del jugador "<<jugador<<endl;
		cout<<"Introduzca la columna"<<endl;
		cin>>col;
		
		usoDeColumna (tablero, blanco, col, dif);
		
		while (dif==10){
			cout<<"ERROR. La columna est� completa"<<endl;
			cout<<"Intentelo otra vez"<<endl;
			cin>>col;
			
			usoDeColumna (tablero, blanco, col, dif);
		}
		
		pedirColor (jugador, colores, eleccion);
		
		colocarFicha (tablero, colores, col);
	}while (!gana (tablero, colores));
	
	cout<<"Ha ganado el jugador "<<jugador<<"."<<endl;
	
	dibujarMatriz (tablero);
return 0;
}

bool turno (int &a){
	if (a%2==0){
		a/=2;
		return false;
	}else{
		a++;
		return true;
	}
}

void rellenarMatriz(TMatriz &tablero){//Rellena una matriz con un mismo elemento TColor
	for (int i=0;i<TY;i++){
		for (int o=0;o<TX;o++){
			tablero[i][o]=Blanco;
		}
	}
}

void dibujarMatriz (const TMatriz &matriz){//Seg�n el color, muestra la matriz de s�mbolos [.@#]
	for (int i=0;i<TY;i++){
		cout<<i<<"-- ";
		for (int o=0;o<TX;o++){
			switch (matriz[i][o]){
				case Blanco:
					cout<<". ";
					break;
				case Azul:
					cout<<"@ ";
					break;
				case Rojo:
					cout<<"# ";
					break;
			}
		}
		cout<<endl;
	}
	
	cout<<"    ";
	for (int i=0;i<TX;i++){
		cout<<"| ";
	}
	cout<<endl;
	cout<<"    ";
	for (int i=0;i<TX;i++){
		cout<<i<<" ";
	}
}

void usoDeColumna (const TMatriz &matriz, const TColor &cuantos, int nColumna, int &diferentes){//Muestra cuantos elementos diferentes a uno seleccionado hay en una columna de la matriz
	int en;
	string comp1, comp2;
	
	diferentes=0;
	
	if (cuantos==Blanco){
		comp1="Blanco";
	}else{
		if (cuantos==Azul){
			comp1="Azul";
		}else{
			if (cuantos==Rojo){
				comp1="Rojo";
			}else{
				comp1="";
			}
		}
	}
	
	for (int i=0;i<TX;i++){
		en=matriz[i][nColumna];
		switch (en){
			case 0:
				comp2="Blanco";
				break;
			case 1:
				comp2="Azul";
				break;
			case 2:
				comp2="Rojo";
				break;
		}
		if(comp1!=comp2){
			diferentes++;
		}
	}
}

void pedirColor (string color, TColor &colores, TArraycolor &eleccion){//Convierte un char en un enum
	int i=0;
	string cad;
	
	bool correcto=false;
	
	while ((!correcto)&&(i<3)){
		cad=eleccion[i];
		if (cad==color){
			switch (i){
				case 0:
					colores=Blanco;
					correcto=true;
					break;
				case 1:
					colores=Azul;
					correcto=true;
					break;
				case 2:
					colores=Rojo;
					correcto=true;
					break;
			}
		}
		i++;
	}
}

void avanzar (int &fila, int &col, unsigned dir){//Avanza una casilla en una direcci�n en una matriz
	switch (dir){
		case 0://Norte
			fila--;
			break;
		case 1://Noreste
			fila--;
			col++;
			break;
		case 2://Este
			col++;
			break;
		case 3://Sureste
			fila++;
			col++;
			break;
		case 4://Sur
			fila++;
			break;
		case 5://Suroeste
			fila++;
			col--;
			break;
		case 6://Oeste
			col--;
			break;
		case 7://Noroeste
			fila--;
			col--;
			break;
	}
}

void celdasConsecutivas (unsigned &pasos, int fila, int col, unsigned dir, const TColor &color, const TMatriz &matriz){//Cuantas casillas del mismo color consecutivas hay en una direcci�n y coordenadas determinadas
	pasos=0;
	
	while ((color==matriz[fila][col])&&(col<TX)&&(fila<TY)&&(col>=0)&&(fila>=0)){
		avanzar (fila, col, dir);
		pasos++;
	}
	if ((fila<0)||(fila>=TY)||(col<0)||(col>=TX)){
		dir=(dir+4)%8;
		avanzar (fila, col, dir);
	}
	
	//color=matriz[fila][col];
}

void colocarFicha (TMatriz &tablero, const TColor &jugador, int col){
	unsigned pasos;
	int fila=TY-1;
	TColor blanco=Blanco;
	
	celdasConsecutivas (pasos, fila, col, 4, blanco, tablero);//�Por qu� se repite?
	
	tablero[TY-pasos][col]=jugador;
}

bool cuatroEnRaya (const TMatriz &tablero, const TColor &jugador, int fila, int col){
	bool cuatro=false;
	unsigned pasos;
	int i=0;
	while ((!cuatro)&&(i<8)){
		celdasConsecutivas (pasos, fila, col, i,jugador, tablero);
		if (pasos>=3){
			cuatro=true;
		}
		i++;
	}
return cuatro;
}

bool gana (const TMatriz &tablero, const TColor &jugador){
	bool fin=false;
	int i=0, o=0;
	
	do {
		do {
			fin=cuatroEnRaya (tablero, jugador, i, o);
			o++;
		}while ((o<TX)&&(!fin));
		i++;
	}while ((i<TY)&&(!fin));
return fin;
}
