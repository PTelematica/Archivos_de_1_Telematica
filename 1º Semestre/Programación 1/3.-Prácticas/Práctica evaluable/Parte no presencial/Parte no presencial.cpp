#include <iostream>
#include <string>
using namespace std;

const int TX = 10;
const int TY = 10;

enum TColor { Blanco, Azul, Rojo };

typedef TColor TMatriz[TY][TX];
typedef string TArraycolor[3];

void pedirColor (string color, TColor &colores, TArraycolor &eleccion, bool &correcto);
void rellenarMatriz (TMatriz &matriz, TColor &colores);
void dibujarMatriz (const TMatriz &matriz);
void enumAleatorio (TMatriz &matriz, int i, int o);
void usoDeColumna (const TMatriz &matriz, const TColor &cuantos, int nColumna, int &diferentes);
void avanzar (int &fila, int &col, unsigned dir);
void celdasConsecutivas (unsigned &pasos, int &fila, int &col, unsigned dir, TColor &color, const TMatriz &matriz);
unsigned contarCeldas (const TMatriz &matriz, const TColor &color);
void dibujarLinea (TMatriz &matriz, const TColor &color, int fila, int col, unsigned dir, unsigned cant);

int main (){
	TMatriz matriz, matriz2;
	TColor colores, cuantos;
	TArraycolor eleccion{"Blanco", "Azul", "Rojo"};
	string color;
	bool correcto;
	int nColumna, diferentes, fila, col, colorEnum;
	unsigned dir, pasos, nColor, cant;
	
	do {//Control de errores del color para m2
		cout<<"Introduzca un color (Blanco, Azul o Rojo)"<<endl;
		cin>>color;
		pedirColor (color, colores, eleccion, correcto);
	}while (!correcto);
	
	if (correcto){//Rellenar matriz1
		rellenarMatriz(matriz, colores);
	}
	
	for (int i=0;i<TY;i++){//Mostrar matriz1
		for (int o=0;o<TX;o++){
			cout<<matriz[i][o]<<"  ";
		}
		cout<<endl;
	}
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	for (int i=0;i<TY;i++){//Componer matriz2 aleatoria
		for (int o=0;o<TX;o++){
			enumAleatorio(matriz2, i, o);
		}
	}
	
	dibujarMatriz (matriz2);
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	cout<<"Introduzca una columna"<<endl;
	cin>>nColumna;
	
	while ((nColumna<0)||(nColumna>=TX)){//Control de errores de la columna
		cout<<"ERROR. El n�mero introducido no est� dentro del rango de las columnas"<<endl;
		cout<<"Introduzca una columna"<<endl;
		cin>>nColumna;
	}
	
	do {//Control de errores del color
		cout<<"Introduzca un color (Blanco, Azul o Rojo)"<<endl;
		cin>>color;
		pedirColor (color, cuantos, eleccion, correcto);
	}while (!correcto);
	
	usoDeColumna (matriz2, cuantos, nColumna, diferentes);
	
	cout<<"Hay "<<diferentes<<" colores diferentes al color seleccionado en la columna "<<nColumna<<endl;
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	cout<<"Introduzca unas coordenadas"<<endl;
	cout<<"X: ";
	cin>>col;
	cout<<"Y: ";
	cin>>fila;
	do{//Control de errores de direcci�n
	cout<<"Introduzca la direcci�n"<<endl;
	cin>>dir;
	}while((dir<0)&&(dir>7));
	
	do {//Control de errores del color (cuantos/colores)
		cout<<"Introduzca un color (Blanco, Azul o Rojo)"<<endl;
		cin>>color;
		pedirColor (color, cuantos, eleccion, correcto);
	}while (!correcto);
	
	celdasConsecutivas (pasos, fila, col, dir, cuantos, matriz2);
	
	colorEnum=cuantos;
	
	cout<<"Hay "<<pasos<<" colores iguales consecutivos en esa direcci�n, y el �ltimo color es el "<<eleccion[colorEnum]<<endl;
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	do {//Control de errores del color (cuantos/colores)
		cout<<"Introduzca un color (Blanco, Azul o Rojo)"<<endl;
		cin>>color;
		pedirColor (color, cuantos, eleccion, correcto);
	}while (!correcto);
	
	nColor=contarCeldas (matriz2, cuantos);
	
	cout<<"En la matriz hay "<<nColor<<" colores iguales al dado"<<endl;
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	cout<<"Introduzca coordenadas en la matriz"<<endl;
	cout<<"X: ";
	cin>>col;
	cout<<"Y: ";
	cin>>fila;
	do{//Control de errores de direcci�n
	cout<<"Introduzca la direcci�n"<<endl;
	cin>>dir;
	}while((dir<0)&&(dir>7));
	cout<<"Introduzca el tama�o de la l�nea"<<endl;
	cin>>cant;
	
	do {//Control de errores del color (cuantos/colores)
		cout<<"Introduzca un color (Blanco, Azul o Rojo)"<<endl;
		cin>>color;
		pedirColor (color, colores, eleccion, correcto);
	}while (!correcto);
	
	dibujarLinea (matriz, colores, fila, col, dir, cant);
	
	for (int i=0;i<3;i++){//Dejar espacios (est�tico)
		cout<<endl;
	}
	
	dibujarMatriz (matriz);
return 0;
}

void pedirColor (string color, TColor &colores, TArraycolor &eleccion, bool &correcto){//Convierte un char en un enum
	int i=0;
	string cad;
	
	correcto=false;
	
	while ((!correcto)&&(i<3)){
		cad=eleccion[i];
		if (cad==color){
			switch (i){
				case 0:
					colores=Blanco;
					correcto=true;
					break;
				case 1:
					colores=Azul;
					correcto=true;
					break;
				case 2:
					colores=Rojo;
					correcto=true;
					break;
			}
		}
		i++;
	}
}

void rellenarMatriz(TMatriz &matriz, TColor &colores){//Rellena una matriz con un mismo elemento TColor
	for (int i=0;i<TY;i++){
		for (int o=0;o<TX;o++){
			matriz[i][o]=colores;
		}
	}
}

void enumAleatorio (TMatriz &matriz, int i, int o){//Fabrica una matriz aleatoria TMatriz
	int a;
	
	a=rand()%3;
	
	switch (a){
		case 0:
			matriz[i][o]=Blanco;
			break;
		case 1:
			matriz[i][o]=Azul;
			break;
		case 2:
			matriz[i][o]=Rojo;
			break;
	}
}

void dibujarMatriz (const TMatriz &matriz){//Seg�n el color, muestra la matriz de s�mbolos [.@#]
	for (int i=0;i<TY;i++){
		cout<<i<<"-- ";
		for (int o=0;o<TX;o++){
			switch (matriz[i][o]){
				case Blanco:
					cout<<". ";
					break;
				case Azul:
					cout<<"@ ";
					break;
				case Rojo:
					cout<<"# ";
					break;
			}
		}
		cout<<endl;
	}
	
	cout<<"    ";
	for (int i=0;i<TX;i++){
		cout<<"| ";
	}
	cout<<endl;
	cout<<"    ";
	for (int i=0;i<TX;i++){
		cout<<i<<" ";
	}
}

void usoDeColumna (const TMatriz &matriz, const TColor &cuantos, int nColumna, int &diferentes){//Muestra cuantos elementos diferentes a uno seleccionado hay en una columna de la matriz
	//int en;
	string comp1, comp2;
	
	diferentes=0;
	
	if (cuantos==Blanco){
		comp1="Blanco";
	}else{
		if (cuantos==Azul){
			comp1="Azul";
		}else{
			if (cuantos==Rojo){
				comp1="Rojo";
			}else{
				comp1="";
			}
		}
	}
	
	for (int i=0;i<TX;i++){
		en=matriz[i][nColumna];
		switch (en){
			case 0:
				comp2="Blanco";
				break;
			case 1:
				comp2="Azul";
				break;
			case 2:
				comp2="Rojo";
				break;
		}
		if(comp1!=comp2){
			diferentes++;
		}
	}
}

void avanzar (int &fila, int &col, unsigned dir){//Avanza una casilla en una direcci�n en una matriz
	switch (dir){
		case 0://Norte
			fila--;
			break;
		case 1://Noreste
			fila--;
			col++;
			break;
		case 2://Este
			col++;
			break;
		case 3://Sureste
			fila++;
			col++;
			break;
		case 4://Sur
			fila++;
			break;
		case 5://Suroeste
			fila++;
			col--;
			break;
		case 6://Oeste
			col--;
			break;
		case 7://Noroeste
			fila--;
			col--;
			break;
	}
}

void celdasConsecutivas (unsigned &pasos, int &fila, int &col, unsigned dir, TColor &color, const TMatriz &matriz){//Cuantas casillas del mismo color consecutivas hay en una direcci�n y coordenadas determinadas
	pasos=0;
	while (color==matriz[fila][col]){
		avanzar (fila, col, dir);
		pasos++;
	}
	if ((fila<0)||(fila>=TY)||(col<0)||(col>=TX)){
		dir=(dir+4)%8;
		avanzar (fila, col, dir);
		color=matriz[fila][col];
	}else{
		color=matriz[fila][col];
	}
}
unsigned contarCeldas (const TMatriz &matriz, const TColor &color){//Cu�ntas casillas del mismo color hay en una matriz
	unsigned n=0;
	for (int i=0;i<TY;i++){
		for (int o=0;o<TX;o++){
			if (matriz[i][o]==color){
				n++;
			}
		}
	}
	return n;
}

void dibujarLinea (TMatriz &matriz, const TColor &color, int fila, int col, unsigned dir, unsigned cant){//Dibuja una l�nea de un color determinado en una matriz
	unsigned pasos=0;
	
	while ((pasos<cant)&&(fila<TY)&&(col<TX)){
		matriz[fila][col]=color;
		avanzar (fila, col, dir);
		pasos++;
	}
}
