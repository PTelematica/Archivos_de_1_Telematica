#include <iostream>
using namespace std;

const float IVA=0.12;
const float DESC=0.05;

int main(){
	float p,n,pIva,pFinal;
	
	cout<<"Introduzca el precio"<<endl;
	cin>>p;
	cout<<"Introduzca el n�mero de unidades"<<endl;
	cin>>n;
	
	pIva=(p+p*IVA)*n;
	
	if (pIva>300){
		pFinal=pIva-pIva*DESC;
	}else{
		pFinal=pIva;
	}
	
	cout<<"El precio final es "<<pFinal<<endl;
return 0;
}
