#include <iostream>

using namespace std;


int main(){
        double pobIni, zomIni, totalIni, pSup, pZom;
        double contag, DEFEN;
        int dias;
		
        cout << "Población inicial:";
        cin >> pobIni;
		
        cout << "Zombis inicial:";
        cin >> zomIni;
		
        cout << "Dias simulación:";
        cin >> dias;
        
        cout << "Capacidad de contagio:";
        cin >> contag;
		
        cout << "Capacidad de defensa:";
        cin >> DEFEN;
        
        totalIni = pobIni + zomIni;
        pSup = pobIni / totalIni;
        pZom = zomIni / totalIni;
		
        for( int d = 0; d < dias; d++ ){
                for( int h = 0; h < 24; h++ ){
                        double dS1, dZ1, nS, nZ, dS2, dZ2, dS3, dZ3, dS4, dZ4;
                        dS1 = -contag * pSup * pZom;
                        dZ1 = contag * pSup * pZom - pZom / DEFEN;
						
                        nS = pSup + (dS1 / 24) / 2;
                        nZ = pZom + (dZ1 / 24) / 2;
						
                        dS2 = -contag * nS * nZ;
                        dZ2 = contag * nS * nZ - nZ / DEFEN;
						
                        nS = pSup + (dS2 / 24) / 2;
                        nZ = pZom + (dZ2 / 24) / 2;
						
                        dS3 = -contag * nS * nZ;
                        dZ3 = contag * nS * nZ - nZ / DEFEN;
						
                        nS = pSup + (dS3 / 24);
                        nZ = pZom + (dZ3 / 24);
						
                        dS4 = -contag * nS * nZ;
                        dZ4 = contag * nS * nZ - nZ / DEFEN;
						
                        pSup = pSup + (dS1 / 6 + dS2 / 3 + dS3 / 3 + dS4 / 6) / 24;
                        pZom = pZom + (dZ1 / 6 + dZ2 / 3 + dZ3 / 3 + dZ4 / 6) / 24;
						
                        if (contag > 0.5){
                            contag = contag - 0.02;
                        }
                        
                        cout << "D: " << d+1 << " H: " << h+1 << " S: " << long( totalIni * pSup) << " Z: " << long( totalIni * pZom ) << endl;
                }
        }
}