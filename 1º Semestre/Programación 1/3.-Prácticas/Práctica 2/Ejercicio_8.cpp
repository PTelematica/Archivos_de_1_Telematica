#include <iostream>
using namespace std;

int main (){
	int n,c;
	
	do{
		cout<<"Introduzca un n�mero par, positivo y menor que 20"<<endl;
		cin>>n;
		
		if ((n<=0)||(n>20)||(n%2!=0)){
			cout<<"ERROR. El n�mero debe ser par, positivo y menor que 20"<<endl;
		}
	}while ((n<=0)||(n>20)||(n%2!=0));
	
	for (int i=0;i<n/2;i++){
		c=i+1;
		
		for (int o=0;o<i;o++){
			cout<<" ";
		}
		
		for (int e=n/2;e<n-i;e++){
			cout<<c++;
		}
		
		c-=2;
		
		for (int e=n/2+1;e<n-i;e++){
		cout<<c--;
		}
		cout<<endl;
	}
	
	for (int i=0;i<n/2;i++){
		c=n/2-i;
		
		for (int o=(n/2)+1;o<n-i;o++){
			cout<<" ";
		}
		
		for (int e=0;e<i+1;e++){
			cout<<c++;
		}
		
		c-=2;
		
		for (int e=0;e<i;e++){
			cout<<c--;
		}
		cout<<endl;
	}
return 0;
}
