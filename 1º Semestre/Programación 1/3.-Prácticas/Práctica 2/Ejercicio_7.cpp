#include <iostream>
using namespace std;

int main (){
	unsigned n,dec,par=0,non=0;
	
	cout<<"Introduzca un n�mero de varios d�gitos"<<endl;
	cin>>n;
	
	while (n!=0){
		dec=n%10;
		
		if (dec%2==0){
			par++;
		}else{
			non++;
		}
		
		n=n/10;
	}
	
	cout<<"En el n�mero introducido hay "<<par<<" d�gitos pares y "<<non<<" d�gitos impares"<<endl;
return 0;
}
