#include <iostream>

using namespace std;

const int TY=7;
const int TX=8;

typedef char TRombo[TY][TX];

void dibujar_rombo (TRombo &rombo, int x, int y, int d);

int main (){
	int x, y, d;
	TRombo rombo;
	
	cout<<"Introduzca el centro del rombo"<<endl;
	cout<<"X: ";
	cin>>x;
	cout<<"Y: ";
	cin>>y;
	cout<<"Introduzca la distancia de los vertices al centro"<<endl;
	cin>>d;
	
	dibujar_rombo (rombo, x, y, d);
	
	for (int i=0;i<TY;i++){
		for (int o=0;o<TX;o++){
			cout<<rombo[i][o];
		}
		cout<<endl;
	}
return 0;
}

void dibujar_rombo (TRombo &rombo, int x, int y, int d){
	for (int i=0;i<TY;i++){
		for (int o=0;o<TX;o++){
			if ((i+o+1==x)||(i-o+1==x)||(o-i+1==y)){
				rombo[i][o]='*';
			}else{
				rombo[i][o]='-';
			}
		}
	}
}
